///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file short.c
/// @version 1.0
///
/// Print the characteristics of the "short", "signed short" and "unsigned short" datatypes.
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "int.h"


///////////////////////////////////////////////////////////////////////////////
/// short

/// Print the characteristics of the "int" datatype
void doInt() {
   printf(TABLE_FORMAT_INT, "int", sizeof(int)*8, sizeof(int), INT_MIN, INT_MAX);
}


/// Print the overflow/underflow characteristics of the "int" datatype
void flowInt() {
   int overflow = INT_MAX;
   printf("int overflow: %d + 1 ",overflow++);
   printf("becomes %d\n", overflow);

   int underflow = SHRT_MIN;
   printf("int underflow: %d - 1 ",underflow--);
   printf("becomes %d\n", underflow);

}


///////////////////////////////////////////////////////////////////////////////
/// unsigned short

/// Print the characteristics of the "unsigned int" datatype
void doUnsignedInt() {
   printf(TABLE_FORMAT_INT, "unsigned int", sizeof(unsigned int)*8, sizeof(unsigned int), 0, UINT_MAX);
}

/// Print the overflow/underflow characteristics of the "unsigned short" datatype
void flowUnsignedInt() {
   unsigned int overflow = UINT_MAX;
   printf("unsigned int overflow: %d + 1 ",overflow++);
   printf("becomes %d\n", overflow);

   unsigned int underflow = 0;
   printf("unsigned int underflow: %d - 1 ",underflow--);
   printf("becomes %d\n", underflow);

}

