///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file short.c
/// @version 1.0
///
/// Print the characteristics of the "short", "signed short" and "unsigned short" datatypes.
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "short.h"


///////////////////////////////////////////////////////////////////////////////
/// short

/// Print the characteristics of the "long" datatype
void doLong() {
   printf(TABLE_FORMAT_LONG, "long", sizeof(long)*8, sizeof(long), LONG_MIN, LONG_MAX);
}


/// Print the overflow/underflow characteristics of the "long" datatype
void flowLong() {
   long overflow = LONG_MAX;
   printf("long overflow: %d + 1 ",overflow++);
   printf("becomes %d\n", overflow);

   long underflow = LONG_MIN;
   printf("long underflow: %d - 1 ",underflow--);
   printf("becomes %d\n", underflow);

}


///////////////////////////////////////////////////////////////////////////////
/// unsigned short

/// Print the characteristics of the "unsigned short" datatype
void doUnsignedLong() {
   printf(TABLE_FORMAT_LONG, "unsigned long", sizeof(unsigned long)*8, sizeof(unsigned long), 0, ULONG_MAX);
}

/// Print the overflow/underflow characteristics of the "unsigned long" datatype
void flowUnsignedLong() {
   unsigned long overflow = ULONG_MAX;
   printf("unsigned long overflow: %d + 1 ",overflow++);
   printf("becomes %d\n", overflow);

   unsigned long underflow = 0;
   printf("unsigned long underflow: %d - 1 ",underflow--);
   printf("becomes %d\n", underflow);

}

